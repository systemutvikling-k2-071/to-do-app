module no.ntnu.idatt1002 {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.idatt1002 to javafx.fxml;
    exports no.ntnu.idatt1002;
    exports no.ntnu.idatt1002.controllers;
    exports no.ntnu.idatt1002.exceptions;
    opens no.ntnu.idatt1002.controllers to javafx.fxml;
}