package no.ntnu.idatt1002.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import no.ntnu.idatt1002.*;
import no.ntnu.idatt1002.exceptions.DuplicateTaskException;
import no.ntnu.idatt1002.exceptions.InvalidCategoryException;
import no.ntnu.idatt1002.exceptions.DuplicateCategoryException;
import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import no.ntnu.idatt1002.popups.CategoryPopup;
import no.ntnu.idatt1002.popups.HelpWindow;
import no.ntnu.idatt1002.popups.InfoPopup;
import java.io.IOException;
import java.time.LocalDate;

/**
 * The primary controller of the application. It handles everything with saving/loading to/from file and creating new
 * categories.
 * It also handles requests to add, delete or edit categories or tasks from the other controllers
 * (Task-/CategoryController).
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class PrimaryController {

    protected CategoryList categories; // a list of all categories, with all tasks, that exists in the app.
    private FileHandler file; // the fileHandler that handles file-saving/-loading.

    @FXML
    private AnchorPane primaryAnchorPane; // the pane at which the stylesheet is attached.
    @FXML
    private Label isEmptyLabel; // label that says "No categories added yet".
    @FXML
    private VBox categoryPaneGroup; // the pane at which the new categories show up.
    @FXML
    private Button addCategoryButton,settingsButton,helpButton; // buttons
    @FXML
    private ImageView imgMode;

    private boolean isLightMode = true;

    /**
     * Initializes the app by initiating objects, loading objects from file and adding elements to the screen.
     * Also sets up buttons and their functions.
     */
    @FXML
    private void initialize() {

        file = new FileHandler("category-saves");
        categories = new CategoryList(file.readFromFile());

        // creates a new category for every category loaded from file:
        for (Category category : categories.getList()) {
            createNewCategory(category);
        }

        // When pressed: creates new category-object -> tries to add to list -> calls createNewCategory-mehtod ->...
        //               ...saves data               -> (cancels if: category with same name)
        addCategoryButton.setOnAction(actionEvent -> {
            try {
                Category category = CategoryPopup.show(categories.findAvailableCategoryName(), categories);

                if (category != null) {
                    categories.add(category);
                    createNewCategory(category);
                    saveToFile();
                    System.out.println("Created new category: \"" + category + "\"");
                } else {
                    System.out.println("Category creation canceled.");
                }

            } catch (DuplicateCategoryException | InvalidCategoryException e) {
                System.out.println(e.getMessage());
                InfoPopup.show("Error", "An error occurred.\n" + e.getMessage());
            }
        });
        helpButton.setOnAction(actionEvent -> HelpWindow.show());

        updateIsEmptyLabel();

        // Settings is not implemented in the app yet, so to make it easy we will just hide the button.
        settingsButton.setVisible(false);

        // Uncomment to add test data:
        // addTestData();

    } // END OF INITIALIZE



    /**
     * Removes a category from the list. Changes are saved to file.
     * (Called from the CategoryController of the category to remove).
     *
     * @param category to be removed.
     */
    protected void deleteCategory(Category category) {
        categories.remove(category);
        setCategoryPane(categories);
        System.out.println("Removed category \"" + category + "\"");
        System.out.println("Saving changes...");
        saveToFile();
        updateIsEmptyLabel();
    }

    /**
     * Sets the display to this set of categories (effectively reloads the display).
     *
     * @param categories list of categories to be added.
     */
    private void setCategoryPane(CategoryList categories){
        categoryPaneGroup.getChildren().clear();
        for (Category category : categories.getList()) {
            createNewCategory(category);
        }
    }


    /**
     * Creates a new category:
     * Loads a new category_scene.fxml and adds it to the categoryPaneGroup {@link #categoryPaneGroup}
     * Then fetches the CategoryController for that FXML and sets the category for that controller.
     *
     * Called when loading a category from a file, or when adding a new category with a button
     *
     * @param category the category to be created and added.
     */
    private void createNewCategory(Category category) {
        try {
            // Loads FXML to the categoryPaneGroup
            FXMLLoader loader = new FXMLLoader(App.class.getResource("category_scene" + ".fxml"));
            categoryPaneGroup.getChildren().add(loader.load());
            // Loads the categoryController and adds the category to it
            CategoryController categoryController = loader.getController();
            categoryController.setCategory(category);
            categoryController.setPrimaryController(this);
        } catch (IOException e) {
            e.printStackTrace();
            InfoPopup.show("Error", "An error occurred.\n" + e.getMessage());
        }
        updateIsEmptyLabel();
    }

    /**
     * Saves current data to the file (category-saves.txt).
     * All controllers should be able to call this method.
     */
    public void saveToFile() {
        file.saveToFile(categories.getList());
    }

    /**
     * Opens the settings window.
     * @param event When the settings icon gets clicked
     */
    @FXML
    void OpenSettingsWindow(ActionEvent event) {
        try {
            System.out.println("Opening settings.");
             FXMLLoader loader = new FXMLLoader(App.class.getResource("settings.fxml"));
             Parent root1 = (Parent) loader.load();
             Stage stage = new Stage();
             stage.setTitle("Settings");
             stage.setScene(new Scene(root1));
             stage.show();
            } catch (Exception e) {
            System.out.println("Cant load new window");
        }
    }

    /**
     * Changes between light and dark mode, depending on which mode is already set
     * @param event when the light-/dark-mode icon gets pressed
     */
    public void changeLightDarkMode(ActionEvent event){
        isLightMode = !isLightMode;
        if(isLightMode) {
            setLightMode();
        } else {
            setDarkMode();
        }
    }

    /**
     * Sets the stylesheet of the primary stage to light-mode.css. It also changes the light-/dark-mode icon
     * to a sun.
     */
    private void setLightMode() {
        System.out.println("Setting light mode...");

        // Changing stylesheet
        primaryAnchorPane.getStylesheets().setAll(String.valueOf((App.class.getResource("Styles/light-mode.css"))));

        // Changing icon
        imgMode.setImage(new Image(String.valueOf(App.class.getResource("samples/outline_dark_mode_black_48dp.png"))));
    }

    /**
     * Sets the stylesheet of the primary stage to dark-mode.css. It also changes the ligh-/dark-mode icon to
     * a moon.
     */
    private void setDarkMode() {
        System.out.println("Setting dark mode...");

        // Changing stylesheet
        primaryAnchorPane.getStylesheets().setAll(String.valueOf(App.class.getResource("Styles/dark-mode.css")));

        // Changing icon
        imgMode.setImage(new Image(String.valueOf(App.class.getResource("samples/outline_light_mode_black_48dp.png"))));
    }

    /**
     * Called when a category is added or removed, sets the label visible if the categoryList is empty, and hidden if
     * there are categories in the list.
     * The label tells the user to add a category.
     */
    public void updateIsEmptyLabel() {
        isEmptyLabel.setVisible(categories.isEmpty());
    }

    /**
     * Adds test data if the category-list is empty. It is ignored if the list already contains other content.
     */
    private void addTestData() {
        if (categories.isEmpty()) {
            try {
                Category c1 = new Category("NTNU");

                Task t1c1 = new Task("Final delivery (idatt1002)" , 3, 2,LocalDate.now());
                t1c1.overrideDeadline(LocalDate.parse("2021-04-30"));
                c1.addTask(t1c1);

                Task t2c1 = new Task("Some Report (idatt1002)",2,2, LocalDate.now());
                t2c1.overrideDeadline(LocalDate.parse("2021-06-02"));
                c1.addTask(t2c1);

                Task t3c1 = new Task("Exam mathematical methods (IMAT2001)" , 3, 1, LocalDate.now());
                t3c1.overrideDeadline(LocalDate.parse("2021-05-20"));
                c1.addTask(t3c1);

                Task t4c1 = new Task("Assignment programming (idatt2001)", 2,3, LocalDate.now());
                t4c1.overrideDeadline(LocalDate.parse("2021-06-30"));
                c1.addTask(t4c1);

                Category c2 = new Category("Work");

                Task t1c2 = new Task("Report", 2,2,LocalDate.now());
                t1c2.overrideDeadline(LocalDate.parse("2021-07-20"));
                c2.addTask(t1c2);

                Category c3 = new Category("Personal");

                Task t1c3 = new Task("Dentist appointment",3,1,LocalDate.now());
                t1c3.overrideDeadline(LocalDate.parse("2022-02-02"));
                c3.addTask(t1c3);

                Task t2c3 = new Task("Call mom",2,1,LocalDate.now());
                t2c3.overrideDeadline(LocalDate.parse("2021-05-28"));
                c3.addTask(t2c3);

                Category[] cList = {c1,c2,c3};

                for (Category c : cList) {
                    categories.add(c);
                    createNewCategory(c);
                    saveToFile();
                }

            } catch (InvalidCategoryException | DuplicateTaskException | InvalidTaskException | DuplicateCategoryException e) {
                e.printStackTrace();
            }
        }
    }

}
