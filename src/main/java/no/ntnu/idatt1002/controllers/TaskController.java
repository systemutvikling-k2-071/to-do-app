package no.ntnu.idatt1002.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import no.ntnu.idatt1002.Category;
import no.ntnu.idatt1002.Task;
import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import no.ntnu.idatt1002.popups.ConfirmPopup;
import no.ntnu.idatt1002.popups.TaskEditNamePopup;
import no.ntnu.idatt1002.popups.InfoPopup;

/**
 * A class that controls what happens to the task-pane. Each task-pane has it's own instance of the TaskController.
 * Handles every button that has to do with editing/deleting tasks.
 * Also sends requests to the CategoryController on saving updating and saving changes.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class TaskController {

    private Task task; // the task assigned to this controller.

    @FXML
    private Label nameLabel, statusLabel, priorityLabel, deadlineLabel, finishDateLabel, startDateLabel;
    @FXML
    private Button deleteButtonTest, changeNameButton, changeDeadlineButton;
    @FXML
    private DatePicker deadlineDatePicker;
    @FXML
    private ToggleGroup priorityToggleGroup,statusToggleGroup;
    @FXML
    private ToggleButton togglePriorityLow,togglePriorityMedium,togglePriorityHigh;
    @FXML
    private ToggleButton toggleStatusLow,toggleStatusMedium,toggleStatusHigh;

    private final String[] priorityColors = {"#B6D7A8","#FFE599","#E06666"}; // colors for priority color-coding
    private final String[] statusColors = {"#A5A5A5","#FFE599","#B6D7A8"}; // colors for status color-coding
    private final String[] deadlineColors = {"#E06666","#FFE599","#B6D7A8"}; // colors for deadline color-coding

    private CategoryController categoryController; // the categoryController of the category that contains the
                                                      // task that this taskController controls.

    /**
     * Initializes the TaskController and sets up it's button-logic.
     */
    @FXML
    public void initialize() {
        deleteButtonTest.setOnAction(actionEvent -> {
            if (ConfirmPopup.show("Delete task", "Are you sure you want to delete this task?")) {
                categoryController.deleteTask(task);
            } else {
                System.out.println("Canceled deletion of task: \"" + task + "\"");
            }
        });
        changeNameButton.setOnAction(actionEvent -> {
            String name = TaskEditNamePopup.show(getCategory(),task);
            try {
                if (name != null) {
                    System.out.println("Changing name of task \"" + task + "\" to \"" + name + "\"");
                    task.setName(name);
                    updateName();
                } else {
                    System.out.println("Tasks name change canceled.");
                }
            } catch (InvalidTaskException e) {
                InfoPopup.show("Error" , "An unexpected error occurred:\n" + e.getMessage());
            }
        });

        statusToggleGroup.selectedToggleProperty().addListener((obsVal, oldVal, newVal) -> {
            if (newVal == null)
                oldVal.setSelected(true);
        });

        priorityToggleGroup.selectedToggleProperty().addListener((obsVal, oldVal, newVal) -> {
            if (newVal == null)
                oldVal.setSelected(true);
        });

        // Tried to generalize this but it didn't work.
                    // PriorityToggleGroup.getToggles().forEach(...), etc.
        // priority-toggles initialize
        togglePriorityLow.setOnAction(actionEvent -> {
            task.setPriority(1);
            updatePriority();
        });
        togglePriorityMedium.setOnAction(actionEvent -> {
            task.setPriority(2);
            updatePriority();
        });
        togglePriorityHigh.setOnAction(actionEvent -> {
            task.setPriority(3);
            updatePriority();
        });

        // status-toggles initialize
        toggleStatusLow.setOnAction(actionEvent -> {
            task.setStatus(1);
            updateStatus();
        });
        toggleStatusMedium.setOnAction(actionEvent -> {
            if (task.getStartDate() == null) { // if start date is not set
                if (ConfirmPopup.show("Start task", "Do you want to start doing this task?\n" +
                        "This action will set the start date of the task to the current date.")) {
                    task.startTask();
                    task.setStatus(2);
                    updateStartDate();
                    updateStatus();
                }
            } else {
                task.setStatus(2);
                updateStatus();
            }
        });
        toggleStatusHigh.setOnAction(actionEvent -> {
            if (task.getFinishDate() == null) { // if finished date is not set
                if (ConfirmPopup.show("Finish task", "Do you want to finish this task?\n" +
                        "This action will set the finish date of the task to the current date.")) {
                    task.setStatus(3);
                    task.finishTask();
                    updateFinishDate();
                    updateStatus();
                }
            } else {
                task.setStatus(3);
                updateStatus();
            }
        });

        changeDeadlineButton.setOnAction(actionEvent -> {
            changeDeadlineButton.setVisible(false);
            deadlineDatePicker.setVisible(true);
        });

        deadlineDatePicker.setOnAction(actionEvent -> {
            try {
                task.setDeadline(deadlineDatePicker.getValue());
                updateDeadline();
            } catch (InvalidTaskException e) {
                System.out.println("Task not edited: " + e.getMessage());
                deadlineDatePicker.setValue(task.getDeadline());
                InfoPopup.show("Invalid date", "Deadline can't be set to before the current date");
            }
        });

    } // END OF MAIN INITIALIZE-METHOD

    /**
     * Gets task from CategoryController and updates labels.
     * This method will initialize the TaskController from the CategoryController.
     *
     * @param task - from CategoryController
     */
    protected void setTask(Task task) {
        this.task = task;
        updateDisplay();
        priorityToggleGroup.getToggles().get(task.getPriority()-1).setSelected(true); // sets default selected priority-button
                                                                                      // corresponding to task-priority.
        statusToggleGroup.getToggles().get(task.getStatus()-1).setSelected(true);

        deadlineDatePicker.setValue(task.getDeadline());
    }

    /**
     * Gets reference to the categoryController of the category containing this task.
     * Is used to initialize the this TaskController from the CategoryController.
     * (TaskController needs this reference to function properly).
     *
     * @param categoryController reference to categoryController.
     */
    protected void setCategoryController(CategoryController categoryController) {
        this.categoryController = categoryController;
    }
    // END OF OTHER INITIAL METHODS


    /**
     * @return the category that contains this task
     */
    public Category getCategory() {
        return categoryController.getCategory();
    }



    // Task-editing and updating methods:

    /**
     * Updates the name of the task in the CategoryController and updates display.
     */
    private void updateName() {
        categoryController.setTaskName(task);
        updateDisplay();
    }

    /**
     * Updates the priority of the task in the CategoryController and updates display.
     */
    private void updatePriority() {
        categoryController.setTaskPriority(task);
        updateDisplay();
    }

    /**
     * Updates the status of the task in the CategoryController and updates display.
     */
    private void updateStatus() {
        categoryController.setTaskStatus(task);
        updateDisplay();
    }

    /**
     * Updates the deadline of the task in the CategoryController and updates display.
     */
    private void updateDeadline() {
        categoryController.setTaskDeadline(task);
        updateDisplay();
    }

    /**
     * Updates the startDate of the task in the CategoryController and updates display.
     */
    private void updateStartDate() {
        categoryController.setTaskStartDate(task);
        updateDisplay();
    }

    /**
     * Updates the finishDate of the task in the CategoryController and updates display.
     */
    private void updateFinishDate() {
        categoryController.setTaskFinishDate(task);
        updateDisplay();
    }

    /**
     * Updates all the colors in the display, like low-priority tasks should have green-ish color,
     * if deadline is close the task should have a red color.
     */
    private void updateColors() {
        changeLabelColor(priorityLabel, priorityColors[task.getPriority() - 1]);
        changeLabelColor(statusLabel, statusColors[task.getStatus() - 1]);
        if (task.getStatus() == 3) {
            changeLabelColor(deadlineLabel, deadlineColors[2]);
        } else if (task.daysUntilDeadline() < 3 && task.daysUntilDeadline() >= 0) {
            changeLabelColor(deadlineLabel, deadlineColors[(int) task.daysUntilDeadline()]);
        } else if (task.daysUntilDeadline() < 0) {
            changeLabelColor(deadlineLabel, deadlineColors[0]);
        } else {
            changeLabelColor(deadlineLabel, deadlineColors[2]);
        }
    }

    /**
     * Changes the color of a label. Used for color coding the task-labels based on priority, status or deadline values.
     * Colors can be formatted as a hexadecimal (string) or color name (string).
     *
     * @param label to change color of.
     * @param color to change to. E.g.: '#FFFFFF', 'black', '#233212'
     */
    private void changeLabelColor(Label label, String color){
        label.setStyle("-fx-background-color: " + color + ";");
    }

    /**
     * Sets all the labels to their appropriate values based on task-data.
     * Called upon task creation or edit.
     */
    private void updateDisplay() {
        nameLabel.setText(task.getName());
        statusLabel.setText(task.getStatusString());
        priorityLabel.setText(task.getPriorityString());
        deadlineLabel.setText(task.getDeadlineString());

        startDateLabel.setText(task.getStartDateAsString());
        finishDateLabel.setText(task.getFinishDateAsString());

        if (task.getStatus() == 3) {
            deadlineDatePicker.setDisable(true);
            changeDeadlineButton.setDisable(true);
        } else {
            deadlineDatePicker.setDisable(false);
            changeDeadlineButton.setDisable(false);
            deadlineDatePicker.setValue(task.getDeadline());
        }

        deadlineLabel.setText(task.daysUntilDeadlineString());

        updateColors();
    }
}
