package no.ntnu.idatt1002.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import no.ntnu.idatt1002.App;
import no.ntnu.idatt1002.Category;
import no.ntnu.idatt1002.Task;
import no.ntnu.idatt1002.TaskSorter;
import no.ntnu.idatt1002.exceptions.InvalidCategoryException;
import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import no.ntnu.idatt1002.exceptions.DuplicateTaskException;
import no.ntnu.idatt1002.popups.*;

import java.io.IOException;
import java.util.ArrayList;

/**
 * A class that controls what happens to the category-pane. Each category-pane has it's own instance of the controller.
 * Handles every button that has to do with category-editing (adding tasks / editing name / removing category).
 * Also handles requests from the TaskController on task-editing/-removal.
 * Sends requests to the PrimaryController on saving and updating changes.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class CategoryController {

    @FXML
    private VBox taskPaneGroup; // the pane at which the new tasks show up.
    @FXML
    private Button sortByNameButton, addTaskButton, sortByPriorityButton, deleteButtonTest, sortByStatusButton,
            sortByDeadlineButton, changeNameButton; // buttons
    @FXML
    private TitledPane root; // the root pane, parent of all other panes.
    @FXML
    private Label isEmptyLabel; // tells the user that the category has no tasks.
    @FXML
    private HBox sortingButtons; // buttons used for sorting, contained in a HBox (hidden when no tasks are present).

    private Category category; // the category that this controller controls.
    private TaskSorter sorter; // a TaskSorter that handles sorting of tasks in a list.
    private PrimaryController primaryController; // reference to the primary controller.

    /**
     * Initializes the CategoryController and it's class-field and buttons.
     */
    @FXML
    private void initialize() {

        root.setStyle("-fx-box-border: transparent;");

        sortingButtons.setVisible(false);

        sorter = new TaskSorter();
        // When clicked: creates a task with name "Task 1", updates category-list (in primaryController), saves data,
        //               and calls createNewTask-Method on the task (visuals)
        addTaskButton.setOnAction(actionEvent -> {
            try { // gets a task from the task-popup.
                Task task = TaskPopup.show(findAvailableTaskName(), category);

                if (task != null) { // if task is valid, adds it to the category and updates the file and the display
                    primaryController.categories.get(category).addTask(task);
                    createNewTask(task);

                    primaryController.saveToFile();
                    System.out.println("Added task \"" + task + "\" to category \"" + category + "\"");
                } else {
                    System.out.println("Task creation canceled.");
                }
            } catch (InvalidTaskException | DuplicateTaskException e) {
                System.out.println("An error occurred.\n" + e.getMessage());
                InfoPopup.show("Error", "An error occurred.\n" + e.getMessage());
            }
        });

        // logic for changing category name
        changeNameButton.setOnAction(actionEvent -> {
            // gets a name from the popup.
            String name = CategoryEditNamePopup.show(primaryController.categories, category);
            try {
                if (name != null) { // if name is valid, update the category and the display.
                    System.out.println("Changing name of category \"" + category + "\" to \"" + name + "\"");
                    category.setName(name);
                    primaryController.categories.get(category).setName(name);
                    root.setText(name);
                    primaryController.saveToFile();
                } else {
                    System.out.println("Category name change canceled.");
                }
            } catch (InvalidCategoryException e) {
                InfoPopup.show("Error", "An unexpected error occurred:\n" + e.getMessage());
            }

        });

        // When clicked: makes a ConfirmPopup that asks "Are you sure...?".
        // Deletes only if "Confirm" is clicked.
        deleteButtonTest.setOnAction(actionEvent -> {
            if (ConfirmPopup.show("Delete category","Are you sure you want to delete this category?\nTasks in the " +
                    "category " +
                    "will also be deleted.")) {
                primaryController.deleteCategory(category);
            } else {
                System.out.println("Canceled deletion of category: \"" + category + "\"");
            }
        });

    } // END OF INITIALIZE-METHOD

    /**
     * Called upon creation of the Category (called in PrimaryController).
     * Used only for initializing the CategoryController.
     *
     * @param c category to be assign to this CategoryController.
     */
    protected void setCategory(Category c) {
        this.category = c;
        root.setText(category.getName());

        System.out.println("Created category: \"" + category.getName() + "\" with " + category.getTasks().size() + " " +
                "tasks");

        for (Task task : category.getTasks()) {
            createNewTask(task);
        }

    }

    /**
     * Gets a reference to the primaryController. (Called from primaryController in createNewCategory()).
     * {@link PrimaryController}
     * Used only for initializing the CategoryController.
     *
     * @param primaryController the main controller.
     */
    protected void setPrimaryController(PrimaryController primaryController) {
        this.primaryController = primaryController;
    }

    // INITIALIZATION ABOVE


    /**
     * Find a generic name for a task that is also available.
     * For making task-name suggestions in add-task.
     *
     * @return a generic, available task name.
     */
    private String findAvailableTaskName() {
        int counter = 1;
        while (true) {
            if (category.isNameAvailable("Task " + counter)) {
                return "Task " + counter;
            }
            else {
                counter++;
            }
        }
    }

    /**
     * Checks if category list is empty and updates "empty"-label accordingly
     */
    private void updateIsEmptyLabel() {
        // Maybe switch out with observer?
        isEmptyLabel.setVisible(category.isEmpty());

        sortingButtons.setVisible(!category.isEmpty());
    }

    /**
     * Gets the category contained in this class.
     *
     * @return controllers category.
     */
    protected Category getCategory() {
        return category;
    }




    // METHODS INVOLVING TASK EDITING BELOW

    /**
     * Sets the finishDate of the task in the category that corresponds to the input task to the input tasks finishDate.
     * @param task to update.
     */
    protected void setTaskFinishDate(Task task) {
        primaryController.categories.get(category).get(task).setFinishDate(task.getFinishDate());
        System.out.println("Task \"" + task + "\" in category \"" + category + "\" edited.\nSaving changes...");
        primaryController.saveToFile();
    }

    /**
     * Sets the startDate of the task in the category that corresponds to the input task to the input tasks startDate.
     * @param task to update.
     */
    protected void setTaskStartDate(Task task) {
        primaryController.categories.get(category).get(task).setStartDate(task.getStartDate());
        System.out.println("Task \"" + task + "\" in category \"" + category + "\" edited.\nSaving changes...");
        primaryController.saveToFile();
    }

    /**
     * Sets the name of the task in the category that corresponds to the input task to the input tasks name.
     * Fails if the name is invalid.
     * @param task to update.
     */
    protected void setTaskName(Task task) {
        try {
            primaryController.categories.get(category).get(task).setName(task.getName());
        } catch (InvalidTaskException e) {
            InfoPopup.show("Critical error", "An error occurred, changes not saved: " + e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Task \"" + task + "\" in category \"" + category + "\" edited.\nSaving changes...");
        primaryController.saveToFile();
    }

    /**
     * Sets the priority of the task in the category that corresponds to the input task to the input tasks priority.
     * @param task to update.
     */
    protected void setTaskPriority(Task task) {
        primaryController.categories.get(category).get(task).setPriority(task.getPriority());
        System.out.println("Task \"" + task + "\" in category \"" + category + "\" edited.\nSaving changes...");
        primaryController.saveToFile();
    }

    /**
     * Sets the status of the task in the category that corresponds to the input task to the input tasks status.
     * @param task to update.
     */
    protected void setTaskStatus(Task task) {
        primaryController.categories.get(category).get(task).setStatus(task.getStatus());
        System.out.println("Task \"" + task + "\" in category \"" + category + "\" edited.\nSaving changes...");
        primaryController.saveToFile();
    }

    /**
     * Sets the deadline of the task in the category that corresponds to the input task to the input tasks deadline.
     * It fails if the deadline is invalid.
     * @param task to update.
     */
    protected void setTaskDeadline(Task task) {
        try {
            primaryController.categories.get(category).get(task).setDeadline(task.getDeadline());
        } catch (InvalidTaskException e) {
            InfoPopup.show("Critical error", "An error occurred, changes not saved: " + e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Task \"" + task + "\" in category \"" + category + "\" edited.\nSaving changes...");
        primaryController.saveToFile();
    }

    // METHODS INVOLVING TASK EDITING ABOVE



    /**
     * Creates a new task:
     * Loads a new task.fxml and adds it to the taskPaneGroup {@link #taskPaneGroup}
     * Then fetches the TaskController for that FXML and sets the task for that controller.
     *
     * Called when loading a task from a file, or when adding a new task with a button
     *
     * @param task the task to be created and added.
     */
    private void createNewTask(Task task) {
        try {
            // Loads FXML to the taskPaneGroup
            FXMLLoader loader = new FXMLLoader(App.class.getResource("task_scene" + ".fxml"));
            taskPaneGroup.getChildren().add(loader.load());
            // Loads the taskController and adds the task to it
            TaskController taskController = loader.getController();
            taskController.setTask(task);
            taskController.setCategoryController(this);
        } catch (IOException e) {
            e.printStackTrace();
            InfoPopup.show("Error", "An error occurred.\n" + e.getMessage());
        }
        updateIsEmptyLabel();
    }

    /**
     * Deletes a task from the category:
     * Fetches the CategoryList in PrimaryController and deletes specified task from {@link #category}.
     * Saves the changes to file.
     *
     * @param task to delete.
     */
    protected void deleteTask(Task task) {
        primaryController.categories.get(category).removeTask(task);
        setTaskPane(category.getTasks());
        System.out.println("Removed task \"" + task + "\"");
        System.out.println("Saving changes...");
        primaryController.saveToFile();
        updateIsEmptyLabel();
    }

    /**
     * Takes the tasks displayed in the category and changes them.
     * Used with task-sorting.
     *
     * @param newTasks new tasks
     */
    private void setTaskPane(ArrayList<Task> newTasks){
        taskPaneGroup.getChildren().clear();
        for (Task task : newTasks) {
            createNewTask(task);
        }
    }

    /**
     * Sorts the task pane based on a sorting-button.
     * This method uses the source of the action event to determine which button was pressed and uses a
     * TaskSorter-object to sort the task pane accordingly.
     *
     * @param actionEvent the triggering event - the button press.
     */
    @FXML
    private void sort(ActionEvent actionEvent) {
        // Sends the sorting buttons for the TaskSorter to deal with:
        Button[] sortingButtons = {sortByNameButton,sortByPriorityButton,sortByStatusButton,sortByDeadlineButton};
        ArrayList<Task> sortedList = sorter.sort(category.getTasks(), (Button) actionEvent.getSource(),sortingButtons);
        setTaskPane(sortedList);
    }
}
