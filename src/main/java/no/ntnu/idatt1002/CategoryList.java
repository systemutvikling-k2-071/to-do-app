package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.DuplicateCategoryException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.function.Predicate;

/**
 * This class deals with and handles a list of categories. This classes job is to keep all categories that are added
 * and has methods to prevent any duplicates or unwanted data to be saved.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class CategoryList {
    ArrayList<Category> categories; // list of categories.

    /**
     * Constructor creates a CategoryList object from a Category-ArrayList.
     *
     * @param categories arraylist of categories.
     * @throws IllegalArgumentException if any duplicates are found within the list.
     */
    public CategoryList(ArrayList<Category> categories) throws IllegalArgumentException {
        if (categories == null) {
            this.categories = new ArrayList<>();
        } else if (hasDuplicate(categories)) {
            throw new IllegalArgumentException("Duplicates in list found.");
        } else {
            this.categories = categories;
        }
    }

    /**
     * Removes a category from the list, if it exists.
     *
     * @param category to be removed.
     * @return true if it removed successfully, false if the category was not found.
     */
    public boolean remove(Category category) {
        if (categories.contains(category)) {
            categories.remove(category);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the categories-list.
     *
     * @return categories list.
     */
    public ArrayList<Category> getList() {
        return categories;
    }

    /**
     * Adds a single Category-object to the list.
     *
     * @param category to be added.
     * @throws DuplicateCategoryException if it finds a duplicate category.
     */
    public void add(Category category) throws DuplicateCategoryException {
        if (!categories.contains(category)) {
            categories.add(category);
        } else {
            throw new DuplicateCategoryException("Category duplicate found");
        }
    }

    /**
     * Adds a list of Categories to the list.
     *
     * @param list of categories to be added.
     * @throws DuplicateCategoryException there are any duplicates found between the classes list and the parameter list.
     */
    public void addAll(ArrayList<Category> list) throws DuplicateCategoryException  {
        if(hasDuplicate(list, categories)) {
            throw new DuplicateCategoryException("Duplicate in list found.");
        }
        categories.addAll(list);
    }

    /**
     * Gets a Category at a index in the list.
     *
     * @param index at which the category is in the list.
     * @return Category at given index.
     */
    public Category get(int index) {
        return categories.get(index);
    }

    /**
     * Tries to find a category that equals the input category.
     *
     * @param category to find in list.
     * @return category that equals input category.
     */
    public Category get(Category category) {
        for (Category c : categories) {
            if (c.equals(category)) {
                return c;
            }
        }
        return null;
    }

    /**
     * Checks if any categories has the given name.
     *
     * @param name to check.
     * @return true if it is available, false if not.
     */
    public boolean isNameAvailable(String name) {
        return !categories.stream().map(Category::getName).anyMatch(Predicate.isEqual(name));
    }

    /**
     * Find a generic name for a category that is also available.
     * For making category-name suggestions in add-category.
     *
     * @return a generic, available category name.
     */
    public String findAvailableCategoryName() {
        int counter = 1;
        while (true) {
            if (isNameAvailable("Category " + counter)) {
                return "Category " + counter;
            }
            else {
                counter++;
            }
        }
    }

    /**
     * Checks if there are any duplicate objects in a list.
     *
     * @param listToCheck list to check for duplicates.
     * @return true if list has duplicates, false if not.
     */
    private boolean hasDuplicate(ArrayList<Category> listToCheck) {
        HashSet<Category> set = new HashSet<>(listToCheck);
        return set.size() < listToCheck.size();
    }

    /**
     * Checks if there are any duplicates between two lists.
     *
     * @param listA list to be checked.
     * @param listB other list to be checked.
     * @return true if any duplicates were found, false if not.
     */
    private boolean hasDuplicate(ArrayList<Category> listA, ArrayList<Category> listB) {
        ArrayList<Category> listToCheck = new ArrayList<>();
        listToCheck.addAll(listA);
        listToCheck.addAll(listB);
        return hasDuplicate(listToCheck);
    }

    public boolean isEmpty() {
        return categories.isEmpty();
    }
}
