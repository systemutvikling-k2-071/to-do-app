package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.InvalidCategoryException;
import no.ntnu.idatt1002.exceptions.DuplicateTaskException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Represents a category in which one can sort different tasks in. A category has a name
 * and a list of tasks.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class Category implements Serializable {

    // Object-variables
    private String name;
    private final ArrayList<Task> tasks = new ArrayList<>();

    /**
     * Constructs a category with a given name. Tasks can be added to this category. The name can not be empty or null.
     * @param name name of category, for example "Work".
     * @throws InvalidCategoryException if category name is invalid.
     */
    public Category(String name) throws InvalidCategoryException {
        if (name != null && !name.equals("")) {
            this.name = name;
        } else {
            throw new InvalidCategoryException("Category name can't be empty.");
        }
    }

    /**
     * Looks through all tasks and checks if any has the given name.
     *
     * @param name to check.
     * @return true if name is available, false if not.
     */
    public boolean isNameAvailable(String name) {
        for (Task t : tasks) {
            if (t.getName().equals(name)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Getter
     * @return name of category
     */
    public String getName() {
        return name;
    }

    /**
     * Setter
     * @param name new name of category
     * @throws InvalidCategoryException if name is invalid.
     */
    public void setName(String name) throws InvalidCategoryException {
        if (name != null && !name.equals("")) {
            this.name = name;
        } else {
            throw new InvalidCategoryException("Category name can't be empty");
        }
    }

    /**
     * Getter
     * @return the list of tasks in this category.
     */
    public ArrayList<Task> getTasks() {
        return tasks;
    }

    /**
     * Gets a task by reference.
     *
     * @param task to get from category.
     * @return task that corresponds to input task.
     */
    public Task get(Task task) {
        for (Task t : tasks) {
            if (t.equals(task)) {
                return task;
            }
        }
        return null;
    }

    /**
     * Adds a task to the category. Fails if task already exists.
     *
     * @param task the task that should be added to this category
     * @throws DuplicateTaskException if duplicate task is found.
     */
    public void addTask(Task task) throws DuplicateTaskException {
        if (!tasks.contains(task)) {
            tasks.add(task);
        } else {
            throw new DuplicateTaskException("Task duplicate found");
        }
    }

    /**
     * Removes a task from the category if it is in it. If not, the method will throw an IllegalArgumentException,
     * so that a wrong input can be handled as wished where this method will be used.
     * @param task the task that should be removed from this category
     * @return true if task is removed, false if not found
     */
    public boolean removeTask(Task task) throws IllegalArgumentException {
        if(tasks.contains(task)) {
            tasks.remove(task);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if the category has zero tasks, indicating it's empty
     * @return true if empty, false if not
     */
    public boolean isEmpty() {
        if(tasks.size() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return name;
    }

    /** (auto-generated)
     * Checks if a category has the same name as this category.
     * Categories are identified by their names.
     *
     * @param o category to check.
     * @return true if the input category has the same name as this category.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(name, category.name);
    }

    // auto-generated
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}
