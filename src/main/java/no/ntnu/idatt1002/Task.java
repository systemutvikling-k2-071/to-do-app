package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.InvalidTaskException;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

/**
 * Represent a task that must be done. The task contains information like a descriptive name, a priority, and
 * a status (Not started, doing, done). Tasks should also be able to be edited afterwards.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class Task implements Serializable {

    // Object-variables.
    private String name;
    private int priority;
    private int status;
    private LocalDate deadline;
    private LocalDate startDate;
    private LocalDate finishDate;

    /**
     * Constructor for a Task. When the task is created, it takes in a descriptive name, a priority
     * and a status. The numbers in priority and status must be between 1 and 3.
     * @param name a descriptive name for something the user wished to do. An example would be "Do homework", or
     *             "Work on oblig 3".
     * @param priority a number representing the priority of a task.
     *                 1: Low
     *                 2: Medium
     *                 3: High
     * @param status   a number representing the status of a task.
     *                 1: Not started
     *                 2: Doing
     *                 3: Done
     * @param deadline the deadline of the task, the date at which the task is due.
     * @throws InvalidTaskException if task name or deadline is invalid.
     */
    public Task(String name, int priority, int status, LocalDate deadline) throws InvalidTaskException {
        if (!isValidDate(deadline)) {
            throw new InvalidTaskException("Deadline can't be before current date");
        }
        if (name == null || name.equals("")) {
            throw new InvalidTaskException("Name can not be empty");
        }

        this.name = name;
        this.priority = priority;
        this.status = status;
        this.deadline = deadline;
    }

    // tested by name:
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return name.equals(task.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name + "-(" + deadline + ")";
    }

    /**
     * Getter
     * @return name of task
     */
    public String getName() {
        return name;
    }

    /**
     * Getter
     * @return priority of task in number-value
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Getter
     * @return status of task in number-value
     */
    public int getStatus() {
        return status;
    }

    /**
     * Getter
     * @return priority of task as the priority the number-value represents.
     */
    public String getPriorityString() {
        if(priority == 1) return "Low";
        if(priority == 2) return "Medium";
        return "High";
    }

    /**
     * Getter
     * @return status of task as the status the number-value represents.
     */
    public String getStatusString() {
        if(status == 1) return "Not started";
        if(status == 2) return "In progress";
        return "Done";
    }

    /**
     * Getter
     * @return date of which a task must be done within.
     */
    public LocalDate getDeadline() {
        return deadline;
    }

    public String getDeadlineString() {
        return deadline.toString();
    }

    /**
     * Setter
     * @param name descriptive name of task.
     * @throws InvalidTaskException if name is invalid.
     */
    public void setName(String name) throws InvalidTaskException {
        if (name != null && !name.equals("")) {
            this.name = name;
        } else {
            throw new InvalidTaskException("Task name can not be empty.");
        }
    }

    /**
     * @return a string-version of startDate. If startDate is null, return "Not started".
     */
    public String getStartDateAsString() {
        return "Start date: " + (startDate == null ? "Not started" : startDate.format(DateTimeFormatter.ofPattern("d" +
                ".MM.uuuu")));
    }

    /**
     * @return a string-version of finishDate. If finishDate is null, returns "Not finished".
     */
    public String getFinishDateAsString() {
        return "Finish date: " + (finishDate == null ? "Not finished" :
                finishDate.format(DateTimeFormatter.ofPattern("d.MM.uuuu")));
    }

    /**
     * Sets the startDate to the current date, indicating the task is started.
     */
    public void startTask() {
        this.startDate = LocalDate.now();
    }

    /**
     * Sets the finishDate to the current date, indicating the task is done.
     */
    public void finishTask() {
        if (startDate == null) {
            startTask();
        }
        this.finishDate = LocalDate.now();
    }

    /**
     * Unfinishes the task.
     */
    public void unfinishTask() {
        this.finishDate = null;
    }

    // Getter
    public LocalDate getStartDate() {
        return startDate;
    }

    // Getter
    public LocalDate getFinishDate() {
        return finishDate;
    }

    //Setter
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    //Setter
    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }

    /**
     * Setter
     * @param priority priority of task as a number-value
     *                 1: Low
     *                 2: Medium
     *                 3: High
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Setter
     * @param status status of task as a number-value
     *               1: Not started
     *               2: Doing
     *               3: Done
     */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
     * @param deadline date task must be done before.
     * @throws InvalidTaskException if deadline is invalid.
     */
    public void setDeadline(LocalDate deadline) throws InvalidTaskException {
        if (isValidDate(deadline)) {
            this.deadline = deadline;
        } else {
            throw new InvalidTaskException("Date is not valid");
        }
    }

    /**
     * Calculates how many days there are until the deadline from the current date.
     * @return number of days until deadline
     */
    public long daysUntilDeadline() {
        return ChronoUnit.DAYS.between(LocalDate.now(), deadline);
    }

    /**
     * Returns the number of days until the deadline in a readable format. For example, if there is four days
     * until the deadline, it will return "4 days".
     * Also, if the deadline is the same date as today's date, it will return "Today",
     * and if the deadline is tomorrow it will return "Tomorrow".
     * @return String version of days until deadline
     */
    public String daysUntilDeadlineString() {
        if(status == 3) {
            return "Finished";
        }
        if(daysUntilDeadline() < 0){
            return "Overdue";
        }
        if(daysUntilDeadline() == 0) {
            return "Today";
        }
        if(daysUntilDeadline() == 1) {
            return "Tomorrow";
        }
        return daysUntilDeadline() + " days";
    }

    /**
     * Checks if a date is valid
     * A non valid date is a date in the past.
     * @param date Localdate
     * @return true if the date is valid, false if not.
     */
    public boolean isValidDate(LocalDate date) {
        return !date.isBefore(LocalDate.now());
    }



    // this is for testing:

    /**
     * In the JUnit-tests, the tasks-deadline can't be set to before the current date, so we can't test it. This
     * method fixes that by overriding the deadline without validation.
     * Also for adding test data to the application.
     *
     * @param deadline to change to.
     */
    public void overrideDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

}
