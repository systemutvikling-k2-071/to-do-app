package no.ntnu.idatt1002.popups;

import javafx.scene.control.Alert;

/**
 * A utility class that has a single method for showing a information-dialog.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public final class InfoPopup {

    /**
     * Shows a information-box with a message to the user.
     *
     * @param title of the box.
     * @param message in the box.
     */
    public static void show(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(message);

        alert.showAndWait();
    }
}
