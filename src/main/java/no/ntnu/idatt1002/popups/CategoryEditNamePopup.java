package no.ntnu.idatt1002.popups;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idatt1002.Category;
import no.ntnu.idatt1002.CategoryList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A utility class for showing category-name-change-dialog.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public final class CategoryEditNamePopup {

    /**
     * Shows a dialog for changing category-name.
     *
     * @param categories list of category to see available names.
     * @param category to change name of.
     * @return a new name for the category.
     */
    public static String show(CategoryList categories, Category category) {

        AtomicBoolean canceled = new AtomicBoolean(false);

        // Window setup
        Stage window = new Stage();

        window.setOnCloseRequest(windowEvent -> {
            canceled.set(true);
            window.close();
        });

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Change name");
        window.setMinWidth(250);
        window.setMinHeight(100);
        window.setResizable(true);

        // Labels
        Label label = new Label();
        label.setText("Change from \"" + category.getName() + "\" to:");
        label.setFont(new Font(14));

        Label invalidNameLabel = new Label();
        invalidNameLabel.setText("Name is invalid or already in use.");
        invalidNameLabel.setVisible(false);
        invalidNameLabel.setTextFill(Color.RED);


        TextField inputTextField = new TextField(category.getName());


        Button changeButton = new Button("Change");
        changeButton.setOnAction(actionEvent -> {
            canceled.set(false);
            window.close();
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(actionEvent -> {
            canceled.set(true);
            window.close();
        });

        // Layout
        HBox buttonsHBox = new HBox(10);
        buttonsHBox.getChildren().addAll(inputTextField,changeButton,cancelButton);
        buttonsHBox.setSpacing(7);

        VBox mainVBox = new VBox(10);
        mainVBox.getChildren().addAll(label,buttonsHBox,invalidNameLabel);
        mainVBox.setSpacing(5);
        mainVBox.setPadding(new Insets(7,7,7,7));

        AnchorPane root = new AnchorPane();
        root.getChildren().add(mainVBox);
        root.prefHeight(62);
        root.prefWidth(459);


        // Logic for invalid inputs
        inputTextField.textProperty().addListener((observable, oldV, newV) -> {
            if (!newV.equals(oldV)) {
                boolean isInvalid;
                if (inputTextField.getText().trim().equals(category.getName())) {
                    isInvalid = false;
                } else {
                    isInvalid =
                            !(categories.isNameAvailable(inputTextField.getText().trim()))
                                    || (inputTextField.getText().trim().equals(""))
                                    || (inputTextField.getText() == null);
                }

                if (isInvalid) {
                    changeButton.setDisable(true);
                    invalidNameLabel.setVisible(true);
                } else {
                    changeButton.setDisable(false);
                    invalidNameLabel.setVisible(false);
                }
            }
        });

        // Show window
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.showAndWait();

        if (canceled.get()) {
            return null;
        } else {
            return inputTextField.getText();
        }
    }
}
