package no.ntnu.idatt1002.popups;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idatt1002.Category;
import no.ntnu.idatt1002.CategoryList;
import no.ntnu.idatt1002.exceptions.InvalidCategoryException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A utility class that has a single method for showing a category-dialog for creating categories.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public final class CategoryPopup {

    /**
     * Shows a dialog that returns a category when input from the user.
     *
     * @param initialName name suggestion for the category.
     * @param categories list of categories to see available names.
     * @return a category based on user input.
     * @throws InvalidCategoryException if category is invalid.
     */
    public static Category show(String initialName, CategoryList categories) throws InvalidCategoryException {

        AtomicBoolean canceled = new AtomicBoolean(false);

        Label nameErrorLabel = new Label("Name is invalid or already in use");
        nameErrorLabel.setVisible(false);
        nameErrorLabel.setTextFill(Color.RED);

        // Window setup
        Stage window = new Stage();

        window.setOnCloseRequest(windowEvent -> {
            canceled.set(true);
            window.close();
        });

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add category");
        window.setMinWidth(250);
        window.setMinHeight(100);
        window.setResizable(true);

        Label label = new Label();
        label.setText("Enter name of category: ");
        label.setFont(new Font(14));
        label.setLayoutX(14);
        label.setLayoutY(6);

        TextField inputTextField = new TextField(initialName);

        Button addButton = new Button("Add");
        addButton.setOnAction(actionEvent -> {
            canceled.set(false);
            window.close();
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(actionEvent -> {
            canceled.set(true);
            window.close();
        });

        // Layouts
        HBox innerLayout = new HBox(10);
        innerLayout.getChildren().addAll(inputTextField, addButton, cancelButton);
        innerLayout.setAlignment(Pos.CENTER);
        innerLayout.setSpacing(7);

        VBox outerLayout = new VBox(10);
        outerLayout.getChildren().addAll(label,innerLayout,nameErrorLabel);
        outerLayout.setSpacing(5);
        outerLayout.setPadding(new Insets(7,7,7,7));

        AnchorPane root = new AnchorPane();
        root.getChildren().add(outerLayout);
        root.prefHeight(62);
        root.prefWidth(459);
        root.setPadding(new Insets(0,0,0,0));


        // Logic for invalid name
        inputTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals(oldValue)) {
                // This makes the name invalid:
                boolean isInvalid =
                        !(categories.isNameAvailable(inputTextField.getText().trim()))
                        || (inputTextField.getText().trim().equals(""))
                        || (inputTextField.getText() == null);

                if (isInvalid) {
                    addButton.setDisable(true);
                    nameErrorLabel.setVisible(true);

                } else {
                    addButton.setDisable(false);
                    nameErrorLabel.setVisible(false);
                }
            }
        });

        // Show window
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.showAndWait();

        // After closing
        if (canceled.get()) {
            return null;
        } else {
            return new Category(inputTextField.getText());
        }

    }
}
