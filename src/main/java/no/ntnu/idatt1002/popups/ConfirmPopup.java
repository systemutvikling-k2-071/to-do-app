package no.ntnu.idatt1002.popups;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A utility class that has methods for showing confirmation-dialogs.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public final class ConfirmPopup {

    /**
     * Shows a confirm-box that returns the answer the user gives.
     *
     * @param title of the box.
     * @param message in the box.
     * @return true or false depending on user-input.
     */
    public static boolean show(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }


    /**
     * Alternative, custom confirmation-box. It is not used anymore.
     *
     * @param title of the box.
     * @param message in the box.
     * @return true or false based on the users input.
     */
    public static boolean show2(String title, String message) {
        AtomicBoolean confirmAnswer = new AtomicBoolean(false);

        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(250);

        Label label = new Label();
        label.setText(message);
        label.setFont(new Font(14));

        Button cancelButton = new Button("Cancel");
        Button confirmButton = new Button("Confirm");
        cancelButton.setOnAction(actionEvent -> {
            confirmAnswer.set(false);
            window.close();
        });
        confirmButton.setOnAction(actionEvent -> {
            confirmAnswer.set(true);
            window.close();
        });
        cancelButton.setDefaultButton(true);
        confirmButton.setDefaultButton(false);
        cancelButton.requestFocus();

        HBox buttonBox = new HBox(10);
        buttonBox.getChildren().addAll(cancelButton,confirmButton);
        buttonBox.setSpacing(5);
        buttonBox.setAlignment(Pos.CENTER);

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, buttonBox);
        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(10,10,10,5));

        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        return confirmAnswer.get();
    }
}
