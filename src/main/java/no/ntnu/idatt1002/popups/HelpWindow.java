package no.ntnu.idatt1002.popups;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idatt1002.App;

/**
 * Utility class that opens a info-dialog with help-information and links to documentation.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public final class HelpWindow {

    /**
     * Shows help window with links.
     */
    public static void show() {

        Hyperlink userManualLink = new Hyperlink("https://gitlab.stud.idi.ntnu" +
                ".no/systemutvikling-k2-071/to-do-app/-/wikis/Home/System/User-manual");
        Hyperlink wikiHomeLink = new Hyperlink("https://gitlab.stud.idi.ntnu.no/systemutvikling-k2-071/to-do-app/-/wikis/home");
        wikiHomeLink.setOnAction(App.getOpenLinkWikiHome());
        wikiHomeLink.setFont(new Font(14));
        userManualLink.setOnAction(App.getOpenLinkUserManual());
        userManualLink.setFont(new Font(14));

        String message = "Hey there! You can find our official documentation at:\n";
        String message2 = "If you need help you can click the link below\nto see a user manual for the app:\n";

        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Help");
        window.setResizable(false);

        Label messageLabel = new Label();
        messageLabel.setText(message);
        messageLabel.setFont(new Font(16));

        Label messageLabel2 = new Label();
        messageLabel2.setText(message2);
        messageLabel2.setFont(new Font(16));

        Button closeButton = new Button("Close");
        closeButton.setOnAction(actionEvent -> window.close());
        closeButton.setFont(new Font(15));

        Label titleLabel = new Label();
        titleLabel.setText("Help");
        titleLabel.setFont(new Font(20));

        HBox hBoxTop = new HBox(10);
        hBoxTop.getChildren().add(titleLabel);
        hBoxTop.setStyle("-fx-background-color: " + "#B8D6EB" +";");
        hBoxTop.setPrefWidth(300);
        hBoxTop.setPrefHeight(40);
        hBoxTop.setPadding(new Insets(10,10,10,10));

        Region separatingSpace = new Region();
        separatingSpace.setPrefHeight(15);

        VBox vBoxBottom = new VBox(10);
        vBoxBottom.getChildren().addAll(messageLabel,wikiHomeLink,separatingSpace,messageLabel2,userManualLink,closeButton);
        vBoxBottom.setPadding(new Insets(10,10,10,10));
        messageLabel.setAlignment(Pos.TOP_LEFT);
        closeButton.setAlignment(Pos.BOTTOM_LEFT);

        VBox root = new VBox(10);
        root.getChildren().addAll(hBoxTop,vBoxBottom);
        root.getStylesheets().add(String.valueOf((App.class.getResource("Styles/HelpWindow.css"))));


        Scene scene = new Scene(root);
        window.setScene(scene);
        window.showAndWait();

    }
}
