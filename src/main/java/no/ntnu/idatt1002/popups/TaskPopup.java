package no.ntnu.idatt1002.popups;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import no.ntnu.idatt1002.Category;
import no.ntnu.idatt1002.Task;
import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A utility class for showing a create-task-dialog creates a task based on user input.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public final class TaskPopup {

    /**
     * Shows a dialog for creating a task. Creates a task based on the users input.
     *
     * @param initialName name suggestion for the task (a name that is available).
     * @param category the category that contains the task (to see what names are available)
     * @return a new task based on user input.
     * @throws InvalidTaskException if the task is invalid.
     */
    public static Task show(String initialName, Category category) throws InvalidTaskException {

        // Error labels setup
        Label nameErrorLabel = new Label("Name is invalid or already in use.");
        nameErrorLabel.setTextFill(Color.RED);
        nameErrorLabel.setVisible(false);

        Label dateErrorLabel = new Label("Deadline can not be in the past.");
        dateErrorLabel.setTextFill(Color.RED);
        dateErrorLabel.setVisible(false);


        // Some validation
        AtomicBoolean canceled = new AtomicBoolean(false);
        AtomicBoolean isValidName = new AtomicBoolean(true);
        AtomicBoolean isValidDate = new AtomicBoolean(true);

        AtomicReference<LocalDate> deadline = new AtomicReference<>(LocalDate.now());



        // Window setup
        Stage window = new Stage();

        window.setOnCloseRequest(windowEvent -> {
            canceled.set(true);
            window.close();
        });

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Add task");
        window.setMinWidth(250);
        window.setMinHeight(100);
        window.setResizable(false);

        // Name input
        Label nameLabel = new Label("Enter name of task: ");
        nameLabel.setFont(new Font(14));

        TextField nameInputField = new TextField(initialName);

        // Priority input
        Label priorityLabel = new Label("Priority:");
        nameLabel.setFont(new Font(14));

        ToggleGroup group = new ToggleGroup();

        ToggleButton toggleLow, toggleMedium, toggleHigh;
        toggleLow = new ToggleButton("Low");
        toggleMedium = new ToggleButton("Medium");
        toggleHigh = new ToggleButton("High");

        toggleLow.setToggleGroup(group);
        toggleMedium.setToggleGroup(group);
        toggleHigh.setToggleGroup(group);

        toggleLow.setSelected(true);
        toggleMedium.setSelected(false);
        toggleHigh.setSelected(false);

        // Deadline input
        Label deadlineLabel = new Label("Deadline:");
        deadlineLabel.setFont(new Font(14));

        DatePicker deadlineInput = new DatePicker(LocalDate.now());

        // Buttons
        Button addButton = new Button("Add");
        addButton.setOnAction(actionEvent -> window.close());

        Button cancelButton = new Button("Cancel");
        cancelButton.setOnAction(actionEvent -> {
            canceled.set(true);
            window.close();
        });

        // Layouts
        HBox addCancelHBox = new HBox(10);
        addCancelHBox.getChildren().addAll(addButton,cancelButton);
        addCancelHBox.setSpacing(5);

        HBox toggleButtonPane = new HBox(10);
        toggleButtonPane.getChildren().addAll(toggleLow,toggleMedium,toggleHigh);
        toggleButtonPane.setSpacing(5);

        VBox mainVBox = new VBox();
        mainVBox.getChildren().addAll(nameLabel,nameInputField,nameErrorLabel,priorityLabel,toggleButtonPane,
                deadlineLabel, deadlineInput, dateErrorLabel, addCancelHBox);
        mainVBox.setSpacing(5);
        mainVBox.setPadding(new Insets(5,5,5,5));

        AnchorPane root = new AnchorPane();
        root.getChildren().add(mainVBox);
        root.prefHeight(62);
        root.prefWidth(459);
        root.setPadding(new Insets(5,5,5,5));

        root.contains(10,10);



        // Logic for valid and invalid inputs
        deadlineInput.setOnAction(actionEvent -> {
            // If date is invalid:
            if (deadlineInput.getValue().isBefore(LocalDate.now())) {
                dateErrorLabel.setVisible(true);
                addButton.setDisable(true);
                isValidDate.set(false);
                // If it is valid:
            } else {
                isValidDate.set(true);
                dateErrorLabel.setVisible(false);
                // name must also be valid here:
                if (isValidName.get()) {
                    addButton.setDisable(false);
                }
            }
        });

        // This happens everytime something in the TextField is changed
        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals(oldValue)) {
                // If name is in any way invalid:
                if (!category.isNameAvailable(nameInputField.getText().trim()) ||
                        (nameInputField.getText().trim().equals("") || nameInputField.getText() == null)) {
                    nameErrorLabel.setVisible(true);
                    isValidName.set(false);
                    addButton.setDisable(true);
                    // If it is valid:
                } else {
                    nameErrorLabel.setVisible(false);
                    isValidName.set(true);
                    // Date must also be valid here:
                    if (isValidDate.get()) {
                        addButton.setDisable(false);
                    }
                }
            }
        });


        // Show window
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.showAndWait();

        // After closing
         String name = nameInputField.getText();

        int priority = toggleHigh.isSelected() ?
                3 : (toggleMedium.isSelected() ?
                2 : toggleLow.isSelected() ? 1 : 0);

        if(priority < 1) {
            return null;
        }

        deadline.set(deadlineInput.getValue());

        if (canceled.get()) {
            return null;
        } else {
            return new Task(name,priority,1, deadline.get());
        }
    } // End of show()-method


}
