package no.ntnu.idatt1002;

import java.io.*;
import java.util.ArrayList;

/**
 * Handles everything with saving and loading Category-ArrayLists to and from a file.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class FileHandler {

    private final File file;
    private final String filePathString;
    private final static String directory = "target/classes/no/ntnu/idatt1002/";
    private final static String fileType = ".txt";

    /**
     * Constructor that takes a fileName and opens a file of said name in projects resources.
     * If file is not found, a new file of said name is created.
     *
     * @param fileName name of file to open/create.
     */
    public FileHandler(String fileName) {
        this.file = new File( directory + fileName + fileType);
        this.filePathString = file.getAbsolutePath();
        System.out.println("File opened at: " + filePathString);
    }

    // getter
    public String getFilePathString() {
        return filePathString;
    }

    /**
     * Checks if file is empty.
     *
     * @return true if file is empty, false if not.
     */
    public boolean isEmpty() {
        return file.length() == 0;
    }

    /**
     * Saves an arraylist of type category to the file.
     * (This method overwrites the data already stored in it).
     *
     * @param list to be saved to file.
     * @return true if save is successful, false if not.
     */
    public boolean saveToFile(ArrayList<Category> list) {
        // tries to write the list-object to the file
        try (FileOutputStream fs = new FileOutputStream(filePathString)) {
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(list);
            System.out.println("Successfully saved to file: " + filePathString);
            return true;
            // if it fail it will print out error-message
        } catch (IOException e){
            System.out.println(list.getClass() + " was not saved.");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Reads the file and returns it's content as an arraylist of Category.
     * If the file is empty, it will just return a new ArrayList.
     * It will fail to read if content is of wrong type and will in this case clear the file and returns a new
     * ArrayList.
     *
     * @return list read from file.
     */
    public ArrayList<Category> readFromFile() {
        // if it is empty: stops and returns empty arraylist
        if (isEmpty()) {
            return new ArrayList<>();
        }
        ArrayList<Category> list;
        // Reads an ArrayList-category-object
        try (FileInputStream fi = new FileInputStream(filePathString)) {
            ObjectInputStream oi = new ObjectInputStream(fi);
            list = (ArrayList<Category>) oi.readObject();
            /* if it fails, it will print error, clear file (to remove any bad data) and return a new arraylist (not
            null) */
        } catch (ClassNotFoundException | IOException | ClassCastException e) {
            e.printStackTrace();
            System.out.println("File could not be read and will be cleared at: " + filePathString);
            clear();
            return new ArrayList<>();
        }
        // returns object (list)
        System.out.println(list.getClass() + " was successfully read.");
        return list;
    }

    /**
     * Clears the file and deletes all content by overwriting with an empty string.
     */
    public void clear() {
        try (FileWriter fw = new FileWriter(filePathString, false)) {
            fw.write("");
            System.out.println("File was cleared at: " + filePathString);
        } catch (IOException e){
            System.out.println("Failed to clear file at: " + filePathString);
            e.printStackTrace();
        }
    }


}
