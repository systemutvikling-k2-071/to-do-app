package no.ntnu.idatt1002;

import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * This class deals with mostly everything that has to do with sorting lists of tasks: sorting, reversing,
 * resetting-sorting, etc.
 * It is used by CategoryController-classes to sort the tasks by name, status, priority and deadline.
 * It also keeps track of what the list was sorted by and how many times a sorting-call has been made.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class TaskSorter {
    private int sortingCounter = 0; // counts how many times a sort-call has been made by the same caller in a row.
    private String lastCall = null; // the last call to this method.

    /**
     * Constructor that creates a TaskSorter-object.
     */
    public TaskSorter(){
    }

    /**
     * This method sorts a list based on a command and a sorting-stage (sortingCounter).
     * The method takes in a list to sort and the button that called this method. It takes the name of the button -
     * the command - and sorts according to the command, e.g. if the button-text says "Name" it sorts by name.
     *
     * It also keeps a in mind what 'stage' in the sorting you are:
     * If you call the same command two times, it returns a reversed-sorted list. If called three times it returns an
     * unsorted list.
     * If another command is called the counter is reset, since, e.g. sorting by name shouldn't affect sorting by
     * priority.
     *
     * Additionally, this method also fixes the display of the button - updates the sorting-arrow-label to the
     * appropriate symbol:
     * Ascending: ⮟, descending: ⮝, unsorted: ⮞
     *
     * @param list to be sorted.
     * @param caller the button that called the method.
     * @param sortingButtons the buttons used for sorting. Used for resetting the buttons that are not used.
     * @return a sorted list based on a command and a 'sorting-stage'.
     */
    public ArrayList<Task> sort(ArrayList<Task> list, Button caller, Button[] sortingButtons) {
        String command = caller.getText(); // NOTE: if the text on the button is changed, the command must also be
                                           // changed.
        ArrayList<Task> sortedList;
        Label buttonLabel = (Label) caller.getChildrenUnmodifiable().get(0); // label of the calling button

        // These symbols are used for displaying sorting: ⮟ ⮝ ⮞

        // Resets the label on all buttons but the calling button.
        Arrays.stream(sortingButtons)
                .filter(b -> !caller.equals(b))
                .forEach(b -> {
                    Label label = (Label) b.getChildrenUnmodifiable().get(0);
                    label.setText("⮞");
                });
        // if it is not the same command as last time, reset the counter.
        if (!command.equals(lastCall)) {
            sortingCounter = 0;
        }
        // then update lastCall
        lastCall = command;

        // If sorted three times, you want your original list back.
        // Resets counter and returns list.
        if (sortingCounter == 2) {
            sortingCounter = 0;
            buttonLabel.setText("⮞");
            System.out.println("Unsorted list");
            return list;
        }

        // Sorts according to command
        switch (command) {
            case "Name":
                sortedList = getListByName(list);
                System.out.print("Sorting by name");
                break;
            case "Priority":
                sortedList = getListByPriority(list);
                System.out.print("Sorting by priority");
                break;
            case "Status":
                sortedList = getListByStatus(list);
                System.out.print("Sorting by status");
                break;
            case "Deadline":
                sortedList = getListByDeadline(list);
                System.out.print("Sorting by deadline");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + command);
        }

        // Checks the sortingCounter to see if list should be reversed or not.
        if (sortingCounter == 0) {
            System.out.println();
            buttonLabel.setText("⮟");
            sortingCounter++;
            return sortedList;
        } else if (sortingCounter == 1) {
            buttonLabel.setText("⮝");
            System.out.println(" - reversed");
            sortingCounter++;
            return getReversedList(sortedList);
        } else {
            throw new IllegalStateException("Unexpected value: " + sortingCounter);
        }
    } // END OF MAIN SORT-METHOD


    /**
     * Sorts a list by name.
     *
     * @param tasks to sort.
     * @return sorted list.
     */
    public ArrayList<Task> getListByName(ArrayList<Task> tasks) {
        return (ArrayList<Task>) tasks.stream()
                .sorted(Comparator.comparing(Task::getName,String.CASE_INSENSITIVE_ORDER))
                .collect(Collectors.toList());
    }

    /**
     * Sorts a list by priority.
     *
     * @param tasks to sort.
     * @return sorted list.
     */
    public ArrayList<Task> getListByPriority(ArrayList<Task> tasks) {
        return (ArrayList<Task>) tasks.stream()
                .sorted(Comparator.comparing(Task::getPriority))
                .collect(Collectors.toList());
    }

    /**
     * Sorts a list by status.
     *
     * @param tasks to sort.
     * @return sorted list.
     */
    public ArrayList<Task> getListByStatus(ArrayList<Task> tasks) {
        return (ArrayList<Task>) tasks.stream()
                .sorted(Comparator.comparing(Task::getStatus))
                .collect(Collectors.toList());
    }

    /**
     * Sorts a list by deadline.
     *
     * @param tasks to sort.
     * @return sorted list.
     */
    public ArrayList<Task> getListByDeadline(ArrayList<Task> tasks) {
        // Gets all tasks that are done
        ArrayList<Task> doneTasks = (ArrayList<Task>) tasks.stream()
                .filter(t -> t.getStatus() == 3)
                .collect(Collectors.toList());
        // Filters out all tasks that are done and sorts list
        ArrayList<Task> sorted = (ArrayList<Task>) tasks.stream()
                .filter(t -> t.getStatus() != 3)
                .sorted(Comparator.comparing(Task::getDeadline))
                .collect(Collectors.toList());
        // Adds the done tasks to the back of the list and returns
        sorted.addAll(doneTasks);
        return sorted;
    }

    /**
     * Returns a reversed version of input-list.
     *
     * @param listToReverse list to reverse.
     * @return a reversed list.
     */
    public ArrayList<Task> getReversedList(ArrayList<Task> listToReverse) {
        Collections.reverse(listToReverse);
        return listToReverse;
    }

    /**
     * Takes a list and returns a new list with only the tasks with given status.
     *
     * @param tasks list to filter.
     * @param status to filter by.
     * @return filtered list.
     */
    public ArrayList<Task> filterByStatus(ArrayList<Task> tasks, int status) {
        return (ArrayList<Task>) tasks.stream()
                .filter(task -> task.getStatus() == status)
                .collect(Collectors.toList());
    }
}
