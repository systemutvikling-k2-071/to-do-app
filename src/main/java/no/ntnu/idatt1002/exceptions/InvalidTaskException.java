package no.ntnu.idatt1002.exceptions;

/**
 * Class for handling user error when creating tasks.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class InvalidTaskException extends Exception {
    public InvalidTaskException(String message){
        super(message);
    }
}
