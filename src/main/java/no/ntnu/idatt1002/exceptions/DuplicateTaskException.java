package no.ntnu.idatt1002.exceptions;

/**
 * Class for handling errors when creating a new task.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class DuplicateTaskException extends Exception {
    public DuplicateTaskException(String message) {
        super(message);
    }
}
