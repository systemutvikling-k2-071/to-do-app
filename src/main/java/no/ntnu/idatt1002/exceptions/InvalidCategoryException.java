package no.ntnu.idatt1002.exceptions;

/**
 * Class for handling user error when creating categories.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class InvalidCategoryException extends Exception {
    public InvalidCategoryException(String message) {
        super(message);
    }
}
