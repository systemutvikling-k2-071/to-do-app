package no.ntnu.idatt1002;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import no.ntnu.idatt1002.controllers.PrimaryController;
import java.io.IOException;

/**
 * This is the main class of the application. It sets up everything and load the main stage with the PrimaryController.
 * @author group K2-07
 * @version 1.0 28.04.2021
 */
public class App extends Application {

    private static Scene scene;
    private static EventHandler<ActionEvent> openLinkWikiHome; // link to GitLab-wiki
    private static EventHandler<ActionEvent> openLinkUserManual; // link to GitLab-user manual
    private PrimaryController primaryController; // the main controller of the application.

    /**
     * Sets up the main stage of the application and loads the .fxml for the main scene.
     *
     * The events that are initiated in this method is for the help-window. It opens up the default web-browser of
     * the computer and goes to the official documentation of this project.
     *
     * @param stage main stage of the application, the application window.
     * @throws IOException if it fail to load the .fxml-file.
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(App.class.getResource("primary_scene" + ".fxml"));

        scene = new Scene(loader.load(), 800,500);

        primaryController = loader.getController();

        stage.setScene(scene);
        stage.setTitle("To-Do Application");

        stage.setOnCloseRequest(e -> {
            System.out.println("Saving before exit...");
            primaryController.saveToFile();
            Platform.exit();
            System.exit(0);
        });

        // links for the help-window
        openLinkWikiHome = event -> getHostServices().showDocument("https://gitlab.stud.idi.ntnu.no/systemutvikling-k2-071/to-do-app/-/wikis/home");
        openLinkUserManual = event -> getHostServices().showDocument("https://gitlab.stud.idi.ntnu" +
                ".no/systemutvikling-k2-071/to-do-app/-/wikis/Home/System/User-manual");

        stage.show();
    }

    // Link to GitLab-wiki home page
    public static EventHandler<ActionEvent> getOpenLinkWikiHome() {
        return openLinkWikiHome;
    }

    // Link to user-manual in GitLab-wiki
    public static EventHandler<ActionEvent> getOpenLinkUserManual() {
        return openLinkUserManual;
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    /**
     * Loads a FXML-file from resources.
     *
     * @param fxml name of the file (without .fxml)
     * @return a loaded fxml-file.
     * @throws IOException if the file could not load.
     */
    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * Launches application.
     * @param args main method arguments
     */
    public static void main(String[] args) {
        launch();
    }

}