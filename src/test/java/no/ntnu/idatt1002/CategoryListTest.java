package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.DuplicateCategoryException;
import no.ntnu.idatt1002.exceptions.InvalidCategoryException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class CategoryListTest {
    CategoryList list;

    @BeforeEach
    public void init() {
        list = new CategoryList(fillWithTestData());
    }

    private ArrayList<Category> fillWithTestData() {
        try {
            ArrayList<Category> categories = new ArrayList<>();
            categories.add(new Category("Name"));
            categories.add(new Category("Name 2"));
            categories.add(new Category("Name 3"));
            return categories;
        } catch (InvalidCategoryException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Category> fillWithOtherTestData() {
        try {
            ArrayList<Category> categories = new ArrayList<>();
            categories.add(new Category("Name 4"));
            categories.add(new Category("Name 5"));
            categories.add(new Category("Name 6"));
            return categories;
        } catch (InvalidCategoryException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Test
    @DisplayName("Adding duplicate throws exception")
    public void addingDuplicateCategory() {
        assertThrows(DuplicateCategoryException.class, () -> list.add(new Category("Name")));
        ArrayList<Category> newList = fillWithTestData();
        assertThrows(DuplicateCategoryException.class, () -> list.addAll(newList));
    }

    @Test
    public void removeCategoryTest() throws InvalidCategoryException {
        assertTrue(list.remove(new Category("Name")));
        assertFalse(list.getList().contains(new Category("Name")));
        assertTrue(list.getList().contains(new Category("Name 2")));
        assertTrue(list.getList().contains(new Category("Name 3")));
        assertFalse(list.remove(new Category("Non-existent Category")));
    }

    @Test
    public void getIndexTest() throws InvalidCategoryException {
        assertEquals(new Category("Name"), list.get(0));
    }

    @Test
    public void getCategoryTest() throws InvalidCategoryException {
        assertEquals(new Category("Name"),list.get(new Category("Name")));
    }

    @Test
    public void isNameAvailableTest() {
        assertFalse(list.isNameAvailable("Name"));
        assertTrue(list.isNameAvailable("RandomName"));
    }

    @Test
    public void addingNotThrows() {
        ArrayList<Category> newList = fillWithOtherTestData();
        assertDoesNotThrow(() -> list.addAll(newList));
        assertDoesNotThrow(() -> list.add(new Category("Name 7")));
    }

}
