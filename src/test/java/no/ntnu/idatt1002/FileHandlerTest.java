package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.InvalidCategoryException;
import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import no.ntnu.idatt1002.exceptions.DuplicateTaskException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class FileHandlerTest {
    private FileHandler file;
    private ArrayList<Category> categories;

    @BeforeEach
    public void init() {
        file = new FileHandler("category-test-file");
        categories = new ArrayList<>();
    }

    private void addCategoriesTestData() {
        try {
            Category categoryWithTask = new Category("Category 1");
            try {
                categoryWithTask.addTask(new Task("Name", 1,1, LocalDate.now()));
            } catch (InvalidTaskException | DuplicateTaskException e) {
                System.out.println("Task not added.");
            }
            categories.add(categoryWithTask);
            categories.add(new Category("Category 2"));
            categories.add(new Category("Category 3"));

        } catch (InvalidCategoryException e) {
            System.out.println("Could not create category.");
        }
    }

    private void fillFileWithData() {
        addCategoriesTestData();
        file.saveToFile(categories);
    }

    @Test
    @DisplayName("Saving to file works as expected")
    public void saveToFileTest() {
        addCategoriesTestData();
        assertTrue(file.saveToFile(categories));
        assertDoesNotThrow(() -> file.saveToFile(new ArrayList<>()));
    }

    @Test
    @DisplayName("Reading from file works as expected")
    public void readFromFileTest() {
        fillFileWithData();
        String expectedName = categories.get(0).getName();
        String actualName = file.readFromFile().get(0).getName();
        assertEquals(expectedName,actualName);
        assertDoesNotThrow(() -> file.readFromFile());
    }

    @Test
    @DisplayName("Tasks can be read from file")
    public void getTaskFromFile() {
        fillFileWithData();
        String actualNameOfTask = file.readFromFile().get(0).getTasks().get(0).getName();
        String expectedNameOfTask = categories.get(0).getTasks().get(0).getName();
        assertEquals(expectedNameOfTask, actualNameOfTask);
    }

    @Test
    @DisplayName("Empty file returns new ArrayList")
    public void readingEmptyFileReturnsEmptyArrayList() {
        file.clear();
        int actualSize = file.readFromFile().size();
        assertEquals(0,actualSize);
    }

    @Test
    @DisplayName("Clear method empties the file")
    public void clearTest() {
        fillFileWithData();
        file.clear();
        int actualSize = file.readFromFile().size();
        assertEquals(0,actualSize);
    }

    @Test
    @DisplayName("Corrupted file will clear itself")
    public void corruptedFileClearsItself() throws IOException {
        fillFileWithData();
        assertEquals(categories,file.readFromFile());

        System.out.println("EXPECTED ERROR AFTER THIS MESSAGE:");

        try (FileOutputStream fs = new FileOutputStream(file.getFilePathString())) {
            ObjectOutputStream os = new ObjectOutputStream(fs);
            String notAArrayList = "Gibberish and nonsense";
            os.writeObject(notAArrayList);
        } catch (IOException e) {
            System.out.println("Writing bad object failed...");
            e.printStackTrace();
        }

        ArrayList<Category> newCategories = file.readFromFile();

        assertTrue(file.isEmpty());
        assertNotEquals(categories,newCategories);
    }

}
