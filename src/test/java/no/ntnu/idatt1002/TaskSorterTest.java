package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class TaskSorterTest {
    TaskSorter sorter;
    ArrayList<Task> tasks;
    ArrayList<Task> sortedList;

    Task task1;
    Task task2;
    Task task3;

    @BeforeEach
    public void init() {
        sorter = new TaskSorter();
        tasks = new ArrayList<>();
        sortedList = new ArrayList<>();
        fillTasks();
    }

    private void fillTasks() {
        try {
            task1 = new Task("Task 1",3,1, LocalDate.MAX);
            task2 = new Task("Task 2",2,2,LocalDate.now());
            task3 = new Task("Task 3" , 1,3,LocalDate.now());

            tasks.add(task1);
            tasks.add(task2);
            tasks.add(task3);
        } catch (InvalidTaskException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Sorting by name works as expected")
    public void sortByNameTest() {
        sortedList = sorter.getListByName(tasks);
        assertEquals(task1, sortedList.get(0));
        assertEquals(task2,sortedList.get(1));
        assertEquals(task3,sortedList.get(2));
    }

    @Test
    @DisplayName("Sorting by priority works as expected")
    public void sortByPriorityTest() {
        sortedList = sorter.getListByPriority(tasks);
        assertEquals(task3,sortedList.get(0));
        assertEquals(task2,sortedList.get(1));
        assertEquals(task1,sortedList.get(2));
    }

    @Test
    @DisplayName("Sorting by status works as expected")
    public void sortByStatusTest() {
        sortedList = sorter.getListByStatus(tasks);
        assertEquals(task1,sortedList.get(0));
        assertEquals(task2,sortedList.get(1));
        assertEquals(task3,sortedList.get(2));
    }

    @Test
    @DisplayName("Reversing a list works as expected")
    public void reverseListTest() {
        sortedList = sorter.getReversedList(tasks);
        assertEquals(task3,sortedList.get(0));
        assertEquals(task2,sortedList.get(1));
        assertEquals(task1,sortedList.get(2));
    }

    @Test
    @DisplayName("Reversing a list sorted by name works as expected")
    public void sortByNameThenReverse() {
        sortedList = sorter.getReversedList(sorter.getListByName(tasks));
        assertEquals(task3,sortedList.get(0));
        assertEquals(task2,sortedList.get(1));
        assertEquals(task1,sortedList.get(2));
    }

    @Test
    @DisplayName("Sorting by deadline works as expected")
    public void sortByDeadlineTest() {
        sortedList = sorter.getListByDeadline(tasks);
        assertEquals(task2,sortedList.get(0));
        assertEquals(task1,sortedList.get(1));
        assertEquals(task3,sortedList.get(2)); // this task is done so it comes last
    }

}
