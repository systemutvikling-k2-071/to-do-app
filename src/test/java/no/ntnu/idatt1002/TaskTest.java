package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;

public class TaskTest {
    Task task;

    @BeforeEach
    public void init() throws InvalidTaskException {
        task = new Task("Example", 1,1,LocalDate.now());
    }

    @Test
    @DisplayName("Get status as string")
    public void getStatusStringTest() throws InvalidTaskException {
        Task task = new Task("Example with priority", 3, 1, LocalDate.now());
        assertEquals("Not started", task.getStatusString());
    }

    @Test
    @DisplayName("Valid date does not throw exception")
    public void validDateCheck() {
            assertTrue(task.isValidDate(task.getDeadline()));
    }

    @Test
    @DisplayName("Invalid date throws exception")
    public void inValidDateCheck() {
        assertThrows(InvalidTaskException.class, () -> new Task("Example Task with invalid date",3,1,LocalDate.of(1999
                ,3,19)));
    }

    @Test
    @DisplayName("Overdue task says overdue")
    public void overdueTaskTest() {
        task.overrideDeadline(LocalDate.MIN);
        assertEquals("Overdue",task.daysUntilDeadlineString());
    }

    @Test
    @DisplayName("When task is done, deadline says Finished")
    public void deadlineSaysFinishedWhenStatusSaysDone() {
        task.setStatus(3);
        assertEquals("Finished",task.daysUntilDeadlineString());
    }

    @Test
    public void getPriorityAsStringTest() {
        assertEquals("Low",task.getPriorityString());
        assertEquals("Not started", task.getStatusString());
        task.setPriority(2);
        task.setStatus(2);
        assertEquals("Medium",task.getPriorityString());
        assertEquals("In progress",task.getStatusString());
        task.setPriority(3);
        task.setStatus(3);
        assertEquals("High",task.getPriorityString());
        assertEquals("Done",task.getStatusString());
    }

    @Test
    public void setNameTest() throws InvalidTaskException {
        task.setName("NewName");
        assertEquals("NewName",task.getName());
    }
}
