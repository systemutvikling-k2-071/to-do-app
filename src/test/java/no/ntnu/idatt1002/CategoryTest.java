package no.ntnu.idatt1002;

import no.ntnu.idatt1002.exceptions.InvalidCategoryException;
import no.ntnu.idatt1002.exceptions.InvalidTaskException;
import no.ntnu.idatt1002.exceptions.DuplicateTaskException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import static org.junit.jupiter.api.Assertions.*;


import java.time.LocalDate;

public class CategoryTest {
   Category category;
   Task dupeTask;
   Task newTask;

    @BeforeEach
    public void init() throws InvalidCategoryException, InvalidTaskException, DuplicateTaskException {
        System.out.println("Resetting category...");
        category = new Category("Category");
        dupeTask = new Task("Task 1",1,1,LocalDate.now());
        newTask = new Task("NEW TASK",1,1,LocalDate.now());
        addTestData();
    }

    //Fills category with Tasks
    public void addTestData() {
        try {
            category.addTask(new Task("Task 1", 1, 1, LocalDate.now()));
            category.addTask(new Task("Task 2", 2, 2, LocalDate.now()));
            category.addTask(new Task("Task 3", 3, 3, LocalDate.now()));
            System.out.println("Category filled with test data");
        } catch (DuplicateTaskException | InvalidTaskException e) {
            System.out.println("Adding test data failed.");
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Adding non-dupelicate task does not throw exception")
    public void addTaskTest() {
        assertDoesNotThrow(() -> category.addTask(newTask));
    }

    @Test
    @DisplayName("Adding duplicate task throws exception")
    public void addingDuplicateTaskThrows() {
        assertThrows(DuplicateTaskException.class, () -> category.addTask(dupeTask));
    }

    @Test
    @DisplayName("ssNameAvailable works as expected")
    public void isNameAvailableTest() {
        assertTrue(category.isNameAvailable("An AVAILABLE NAME"));
        assertFalse(category.isNameAvailable("Task 1"));
    }

    @Test
    @DisplayName("Removing task works as expected")
    public void removeTaskTest() {
        assertTrue(category.getTasks().contains(dupeTask));
        assertTrue(category.removeTask(dupeTask));
        assertFalse(category.getTasks().contains(dupeTask));
        assertFalse(category.removeTask(newTask));
    }

    @Test
    @DisplayName("Getting a task works as expected")
    public void getTaskTest() {
        assertEquals(category.get(dupeTask), dupeTask);
        assertNull(category.get(newTask));
    }
}
